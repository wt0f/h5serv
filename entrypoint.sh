#!/bin/bash

cmd=$1
shift

if [[ "$cmd" != "" ]]; then
    # if first arg is an executable then run it
    if [[ -x $cmd ]]; then
        exec $cmd $@
    else
        # search $PATH for $cmd
        for dir in ${PATH//:/ }; do
            if [[ -x "$dir/$cmd" ]]; then
                exec "$dir/$cmd" $@
            fi
        done

        # Check to see if the command is an alias
        if [[ "$cmd" == "server" ]]; then
            cd h5serv
            exec python h5serv --datapath=/data --log_file=
        fi
    fi
fi

# config=''
# if [[ -r /etc/dmrlink.cfg ]]; then
#     config="--config /etc/dmrlink.cfg"
# fi

cd h5serv
exec python h5serv --datapath=/data --log_file= $@
