# h5serv



## h5serv options

Following are acceptable command line args with their defaults.

```
--cors_domain '*'
--domain hdfgroup.org
--hdf5_ext .h5
--new_domain_policy
--debug True
--password_uri
--ssl_cert ''
--ssl_key ''
--ssl_port 6050
--ssl_cert_pwd
--toc_name .toc.h5
--log_level INFO
--log_file h5serv.log
--datapath data
--background_timeout 1000
--port 5000
--allow_noauth True
--home_dir home
--public_dir "['public', 'test']"
--static_path static
--static_url "r'/views/(.*)'"
--mongo_dbname hdfdevtest
```

Any of the above options can also be specified in the environment as an
upper case string (i.e. DOMAIN=wt0f.com)

## Password file

To make the password file, execute the following command inside the running
docker container.

    touch passwd.h5
    docker run -it --rm -v passwd.h5:/usr/src/app/passwd.h5 <h5serv> python h5serv/util/admin/makepwd_file.py

This will create the file `/usr/src/app/passwd.h5` and prepare it for seeding
with passwords.

Passwords can then be created or updated with

    docker exec -it h5serv bash
    python h5serv/util/admin/update_passwd.py [-a|-r] -u USER -e EMAIL -p PASSWD

The `-a` will add a new user and `-r` will update a user.

