FROM python:3-buster

#RUN apk update && apk add alpine-sdk git hdf5-dev
RUN apt-get update -y && apt-get install -y git

WORKDIR /usr/src/app
VOLUME /data

COPY requirements.txt .
COPY entrypoint.sh .
RUN pip install --no-cache -r requirements.txt && \
    git clone --depth=1 https://github.com/HDFGroup/h5serv.git

EXPOSE 5000

ENTRYPOINT ["/usr/src/app/entrypoint.sh"]